#!/bin/bash

targetPlaybook=$1
additionalParams=''

BOLD='\033[1m'
RED='\033[31m'
BLUE='\033[90m'
NC='\033[0m' # No Color

## Help section explains how to use the command
if [ -z $1 ]; then
  echo " ";
  echo -e "  ${RED}Error!${NC} ${BOLD}Missing command parameters.${NC}"
  echo " ";
  echo "  Command format: "
  echo -e "     ${BLUE} ./ansible-run.sh playbook-name-yml [additional-parameters]${NC}"
  echo " ";
  echo "  ---- "
  echo "  Example: "
  echo -e "    ${BLUE}./ansible-run.sh create-ssh-account.yml --check --extra-vars target_username=test${NC}"
  echo -e "    ${BLUE}./ansible-run.sh delete-ssh-account.yml --check --extra-vars target_username=test${NC}"
  echo " ";
  exit 1
fi

## Check for additional params and pass them to ansible-playbook
echo "$@"
i=1
for param in "$@"
do
  if [ $i -gt 1 ]; then
    additionalParams+=" $param";
  fi

  i=$((i + 1))
done

echo " "
echo "  Target Playbook: $targetPlaybook"
echo "  Additional Params: $additionalParams"
echo "  ---- "
echo " "

ansible-playbook -i servers playbooks/$targetPlaybook $additionalParams
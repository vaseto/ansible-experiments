### Ansible experiments repository

#### Technologies used:  
   1. Ansible - mostly
   2. Nomad - not yet 
   3. Consul - not yet

#### Mostly used for learning and bootstrapping some basic server setup operations

#### Inspired by:   
https://medium.com/google-cloud/deploy-nomad-and-consul-using-ansible-on-gcp-478b39e7818b?sk=7ce30b64a78cd767014cc18fe0000319

---
Shorthand usage:
`./ansible-run.sh NAME_OF_PLAYBOOK "[additional parameters]"` - here additional parameters are optional. If multiple parameters are added
wrap them in quotes. _I am too lazy to implement a smart gathering of ${2++} parameters and build a param string._

Current execution: 

Shorthand samples:  
`./ansible-run.sh create-ssh-account.yml "--extra-vars new_user_name=test"`   
`./ansible-run.sh delete-ssh-account.yml "--extra-vars delete_user_name=test"`  

---
Full commands:  
_Create a new user and add login credentials:_

`ansible-playbook -i servers playbooks/create-ssh-account.yml  --extra-vars new_user_name=test`

This command will require a new public key with the name test.pub to be added in `data/public_keys`

---
_Delete an existing account along with the home dir:_
```ansible-playbook -i servers playbooks/delete-ssh-account.yml  --extra-vars delete_user_name=test```

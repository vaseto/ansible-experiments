
_@TODO: Allow password-less sudo execution_

1. Create a template file for "etc/sudoers" on server bootstrapping.
   with the following line appended
   ` %sudo   ALL=(ALL) NOPASSWD:ALL`
   The idea is to allow sudo execution without passwords.
2. Create a copy of the `/etc/sudoers` file on the server named sudoers.backup
3. Overwrite `/etc/sudoers` with the template file.

__OR__
1. On each "create user" operation add append a line
   `{{username}} ALL=(ALL) NOPASSWD:ALL`
   to a file accounts located in `/etc/sudoers.d/accounts`
2.
3. Probably I should cleanup the file when a user is deleted
